# Invoice Creator

Besides the self-explanatory name, this project is an exercise for arrays and DOM manipulation.

Figma project provided by Scrimba at https://www.figma.com/file/roUn8DT7zHTI9tcL2JXNZG/Invoice-Generator?node-id=0%3A1

## Requirements

- Array to hold services requested
- Buttons to add service to array
- Place to display data from array updated everytime the array changes
- Don't charge > once for same service
- Total amount updated
- Button to "Send invoice"(reset)
- Stretch: remove items after adding