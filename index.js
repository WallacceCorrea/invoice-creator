const services = [
  {
    id: 1,
    type: "Wash Car",
    price: 10,
  },
  {
    id: 2,
    type: "Mow Lawn",
    price: 20,
  },
  {
    id: 3,
    type: "Pull Weeds",
    price: 30,
  },
];

const servicesEl = document.querySelector(".srv-buttons");
const listEL = document.querySelector("#items");
const servicesList = [];
const listItemEl = document.querySelector(".task");
const listItemPriceEl = document.querySelector(".total");
const totalSumEl = document.querySelector(".total-sum");
const sendBtnSectionEl = document.querySelector(".send-btn-section");
const removeBtn = document.querySelector(".remove-btn");

let clicked = [false, false, false];

const listItems = [];

function renderApp() {
  renderButtons();
}

function renderButtons() {
  let contentBtn = "";
  for (let i = 0; i < services.length; i++) {
    contentBtn += `<button class="services-btn pointer" value="${services[i].id}">${services[i].type} <span>$</span>${services[i].price}</button>`;
  }
  servicesEl.innerHTML = contentBtn;
}

function checkItem(item) {
  let index = item - 1;
  if (item === 1 && !clicked[index]) {
    addItem(index);
  } else if (item === 2 && !clicked[index]) {
    addItem(index);
  } else if (item === 3 && !clicked[index]) {
    addItem(index);
  } else {
    alert("Item already added");
  }
}

function addItem(index) {
  clicked[index] = true;
  servicesList.push(services[index]);
  displayList();
}

servicesEl.addEventListener("click", function (m) {
  let element = parseInt(m.target.value);
  if (element) {
    checkItem(element);
  }
});

function displayList() {
  let contentType = "";
  let contentPrice = "";
  let contentRemoveButton = "";

  if (servicesList) {
    for (let i = 0; i < servicesList.length; i++) {
      contentType += `<label id="items">${servicesList[i].type}
      <button class="remove-btn pointer" onclick="removeItem(${servicesList[i].id})" >remove</button></label>`;
      contentPrice += `<label id="value"><span id="dollar">$</span>${servicesList[i].price}</label>`;
      contentRemoveButton = `<button class="remove-btn pointer">Remove</button>`;
    }
  }
  listItemEl.innerHTML = contentType;
  listItemPriceEl.innerHTML = contentPrice;
  removeBtn.innerHTML = contentRemoveButton;

  displayTotal(calcTotal());
}

function displayTotal(sum) {
  totalSumEl.innerHTML = `<span>$</span><label class="total-sum" id="sum-value">${sum}</label>`;
}

function calcTotal() {
  let sum = 0;
  for (let i = 0; i < servicesList.length; i++) {
    sum += parseInt(servicesList[i].price);
  }
  return sum;
}

sendBtnSectionEl.addEventListener("click", function () {
  for (let i = 0; i < servicesList.length + 1; i++) {
    servicesList.pop();
  }
  servicesList.pop();
  clicked = [false, false, false];
  displayList();
});

function removeItem(item) {
  for (let i = 0; i < servicesList.length; i++) {
    if (servicesList[i].id === item) {
      servicesList.splice(i, 1);
    }
  }

  displayList();
}
renderApp();
